<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class BigIntegerTest extends TestCase
{
  public function getNumberDataProvider()
  {
    return [
      "1234567890 > 1234567890" => ["123456790", "123456790"],
      "00000001234567890 > 1234567890" => ["123456790", "0000000123456790"],
      "-00000001234567890 > -1234567890" => ["-123456790", "-0000000123456790"],
      "+0 = 0" => ["0", "+0"],
      "-0 = 0" => ["0", "-0"],
    ];
  }

  /**
   * Test if getNumber() returns correct Number
   *
   * @dataProvider getNumberDataProvider
   *
   * @param $expected
   * @param $value
   */
  public function testGetNumber( $expected, $value )
  {
    $bigInt = new BigInteger($value);
    $this->assertEquals ($expected, $bigInt->getNumber ());
  }

  /**
   * Test if incorrect argument throws InvalidArgumentException
   */
  public function testGetNumber_InvalidValue_ThrowsException()
  {
    $this->expectException (InvalidArgumentException::class);
    new BigInteger("+21312312d2323323");
  }

  /**
   * Test if getDigits() works correctly
   */
  public function testGetDigits()
  {
    $str = "1234567890";
    $bigInt = new BigInteger($str);
    $this->assertIsArray ($bigInt->getDigits ());
    $this->assertEquals (strlen ($str), count ($bigInt->getDigits ()));
    $this->assertEquals ($str, implode ("", array_reverse ($bigInt->getDigits ())));
  }

  public function getSignDataProvider()
  {
    return [
      "-1333" => [BigInteger::SIGN_NEGATIVE, "-1333"],
      "-0" => [BigInteger::SIGN_ZERO, "-0"],
      "0" => [BigInteger::SIGN_ZERO, "0"],
      "2323" => [BigInteger::SIGN_POSITIVE, "2323"],
      "+2323" => [BigInteger::SIGN_POSITIVE, "+2323"],
    ];
  }

  /**
   * Test if getSign() works correctly
   *
   * @dataProvider getSignDataProvider
   *
   * @param $expected
   * @param $value
   */
  public function testGetSign( $expected, $value )
  {
    $bigInt = new BigInteger($value);
    $this->assertEquals ($expected, $bigInt->getSign ());
  }

  public function compareToDataProvider()
  {
    return [
      "1 compared with -2 return -1" => [-1, "1", "-2", false],
      "-2 compared with 1 return  1" => [1, "-2", "1", false],
      "10 compared with 2 return -1" => [-1, "10", "2"],
      "2 compared with 10 return  1" => [1, "2", "10"],
      "99 compared with 90 return  -1" => [-1, "99", "90"],
      "-20 compared with 2 return  1" => [-1, "-20", "2"],
      "4 compared with 3 return  -1" => [-1, "4", "3"],
      "99998 compared with 99999 return  1" => [1, "99998", "99999"],
      "99998 compared with -99999 return  1" => [1, "99998", "-99999"],
      "999999999999999999999999999 compared with -999999999999999999999999999 return  1" => [0, "999999999999999999999999999", "-999999999999999999999999999"],
    ];
  }

  /**
   * Test if add works correctly
   *
   * @dataProvider compareToDataProvider
   *
   * @param $expected
   * @param $value1
   * @param $value2
   * @param bool $ignoreSign
   */
  public function testCompareTo( $expected, $value1, $value2, $ignoreSign = true )
  {
    $bigInt1 = new BigInteger($value1);
    $bigInt2 = new BigInteger($value2);
    $this->assertEquals ($expected, $bigInt1->compareTo ($bigInt2, $ignoreSign));
  }

  public function addDataProvider()
  {
    return [
      "0 + 1 = 1" => ["1", "0", "1"],
      "1 + 0 = 1" => ["1", "1", "0"],
      "1 + 1 = 2" => ["2", "1", "1"],
      "10 + 1 = 11" => ["11", "10", "1"],
      "10 + 134243 = 11" => ["134253", "10", "134243"],
      "9999999999999999 + 1 = 10000000000000000" => ["10000000000000000", "9999999999999999", "1"],
      "9 + (-1) = 8" => ["8", "9", "-1"],
      "10 + (-1) = 9" => ["9", "10", "-1"],
      "(-99) + 2 = -97" => ["-97", "-99", "2"],
      "(-100) + 2 = -98" => ["-98", "-100", "2"],
    ];
  }

  /**
   * Test if add works correctly
   *
   * @dataProvider addDataProvider
   *
   * @param $expected
   * @param $value1
   * @param $value2
   */
  public function testAdd( $expected, $value1, $value2 )
  {
    $bigInt = new BigInteger($value1);
    $bigInt->add (new BigInteger($value2));
    $this->assertEquals ($expected, $bigInt->getNumber ());
  }

  public function subDataProvider()
  {
    return [
      "2 - 2 = 0" => ["0", "2", "2"],
      "0 - 0 = 0" => ["0", "0", "0"],
      "22 - 0 = 22" => ["22", "22", "0"],
      "0 - 22 = -22" => ["-22", "0", "22"],
      "0 - -22 = 22" => ["22", "0", "-22"],
      "-2 - 22 = -24" => ["-24", "-2", "22"],
      "22 - -2 = -24" => ["24", "22", "-2"],
      "22 - 2 = 20" => ["20", "22", "2"],
      "2 - 22 = -20" => ["-20", "2", "22"],
      "10 - 2 = 8" => ["8", "10", "2"],
      "9999999 - 100000000000000000 = -99999999990000001" => ["-99999999990000001", "9999999", "100000000000000000"],
    ];
  }

  /**
   * Test if sub works correctly
   *
   * @dataProvider subDataProvider
   *
   * @param $expected
   * @param $value1
   * @param $value2
   */
  public function testSub( $expected, $value1, $value2 )
  {
    $bigInt = new BigInteger($value1);
    $bigInt->sub (new BigInteger($value2));
    $this->assertEquals ($expected, $bigInt->getNumber ());
  }
}

