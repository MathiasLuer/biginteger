<?php declare(strict_types=1);

/**
 * Class BigInteger do mathematical operations on very big integer. with a maximum of PHP_INT_MAX - 1 digits
 */
class BigInteger
{

  /**
   * negative sign
   */
  const SIGN_NEGATIVE = -1;
  /**
   * zero sign
   */
  const SIGN_ZERO = 0;
  /**
   * positive sign
   */
  const SIGN_POSITIVE = 1;

  /**
   * this is BigInteger is bigger
   */
  const THIS_IS_BIGGER = -1;
  /**
   * both are equal
   */
  const BOTH_ARE_EQUAL = 0;
  /*
   * the other BigInteger is bigger
   */
  const THE_OTHER_ONE_IS_BIGGER = 1;

  /**
   * Sign of Number
   * @var int SIGN_NEGATIVE|SIGN_ZERO|SIGN_POSITIVE
   */
  private $sign = self::SIGN_POSITIVE;

  /**
   * Array contains splitted digits in reverse order
   * @var $digits
   */
  private $digits = [];

  /**
   * BigInteger constructor takes an integer string e.g. "-123", "123", "0001"
   *
   * @param string $integerString
   */
  public function __construct( string $integerString )
  {
    $this->setNumber ($integerString);
  }

  /**
   * Gets current value of BigInteger as integer string
   *
   * @return string
   */
  final public function getNumber(): string
  {
    return ($this->sign === self::SIGN_NEGATIVE) ?
      "-" . implode (array_reverse ($this->digits)) : // return negative number
      implode (array_reverse ($this->digits)); // return positive number
  }

  /**
   * Sets value of BigInteger
   *
   * @param string $integerString Integer string e.g. "123", "-333"
   */
  final public function setNumber( string $integerString )
  {
    // check if valid
    if (!is_numeric ($integerString)) {
      throw new InvalidArgumentException("Argument is not valid. Only numbers and signs allowed!");
    }

    // check sign
    if ($integerString[0] == "-") {
      $this->sign = self::SIGN_NEGATIVE;
      $integerString = ltrim ($integerString, '-');
    } elseif ($integerString[0] == "+") {
      $this->sign = self::SIGN_POSITIVE;
      $integerString = ltrim ($integerString, '+');
    }

    // removing leading zeros
    $integerString = ltrim ($integerString, "0");

    // if empty set to zero
    if ($integerString == "") {
      $this->sign = self::SIGN_ZERO;
      $integerString = "0";
    }

    // put integerString into an array in reverse order
    $digits = array_reverse (str_split ($integerString));

    $this->digits = $digits;
  }

  /**
   * Returns the digits array of BigInteger. Mainly for internal use.
   *
   * @return array The digits array of BigInteger in reverse order
   */
  final public function getDigits(): array
  {
    return $this->digits;
  }

  /**
   * Returns the sign of Biginteger. Mainly for internal use.
   *
   * -1 = Biginteger is negative
   *  0 = Biginteger is zero
   *  1 = Biginteger is positive
   *
   * @return int sign of BigInteger
   */
  final public function getSign()
  {
    return $this->sign;
  }

  /**
   * Add two BigIntegers
   *
   * @param BigInteger $other
   */
  public function add( BigInteger $other )
  {
    if ($this->switchToSubIfSignsAreDifferent ($other)) {
      return;
    }

    if ($other->getSign () == self::SIGN_ZERO || $this->ifThisIsZeroTakeOtherValue ($other)) {
      return;
    }

    $this->addDigitByDigit ($other);
  }

  /**
   * Subtract two BigIntegers
   *
   * @param BigInteger $other
   */
  public function sub( BigInteger $other )
  {
    if ($this->ifBothAreSameSetToZero ($other))
      return;

    if ($this->ifThisIsZeroTakeOtherValue ($other, true) ||
        $other->getSign () == self::SIGN_ZERO || // if $other is zero do nothing
        $this->switchToAddIfSignsAreDifferent ($other)) {
      return;
    }

    $this->subDigitByDigit ($other);

    $this->trimLeadingZeroes ();

  }

  /**
   * This function compare this BigInteger with another BigInteger.
   * It returns following integers.
   *
   * -1 this BigInteger is bigger<br/>
   *  0 both are the same size<br/>
   *  1 the other BigInteger is bigger<br/>
   *
   * @param BigInteger $other
   * @param bool $ignoreSign
   * @return int
   */
  final public function compareTo( BigInteger $other, bool $ignoreSign = false ): int
  {
    // try to identify bigger by sign
    if (!$ignoreSign && $this->getSign () != $other->getSign ()) {
      if (($whichIsBigger = $this->identifyBiggerBySign ($other)) != self::BOTH_ARE_EQUAL)
        return $whichIsBigger;
    }

    // try to identify bigger by digit count
    if (($whichIsBigger = $this->identifyBiggerByDigitCount ($other)) != self::BOTH_ARE_EQUAL)
      return $whichIsBigger;

    // compare all digits till one differs or all digits are compared
    return $this->compareDigitByDigit ($other);
  }

  /**
   * Switch the sign of this BigInteger
   */
  final public function switchSign()
  {
    if ($this->getSign () == self::SIGN_ZERO)
      return;

    if ($this->getSign () == self::SIGN_NEGATIVE) {
      $this->sign = self::SIGN_POSITIVE;
    } else {
      $this->sign = self::SIGN_NEGATIVE;
    }
  }


  /*********************************************************************************
   *  Helper Methods
   *********************************************************************************/

  /**
   * This helper function is used internally to filling digits up with zeroes.
   *
   * @param array $digits
   * @param int $countOfDigitsNeeded
   * @return array
   */
  private function fillWithZeros( array $digits, int $countOfDigitsNeeded ): array
  {
    $numOfZeroes = $countOfDigitsNeeded - count ($digits);

    for ($i = 0; $i < $numOfZeroes; $i++) {
      $digits[] = "0";
    }
    return $digits;
  }

  /**
   * Makes digits same length by filling the short one with leading zeros
   *
   * @param $thisDigits
   * @param $otherDigits
   */
  final private function makeDigitsSameLength( &$thisDigits, &$otherDigits ): void
  {
    if (count ($thisDigits) > count ($otherDigits)) {
      $otherDigits = $this->fillWithZeros ($otherDigits, count ($thisDigits));
    } elseif (count ($thisDigits) < count ($otherDigits)) {
      $thisDigits = $this->fillWithZeros ($thisDigits, count ($otherDigits));
    }
  }

  /**
   * This method return true if switched to sub
   *
   * @param BigInteger $other
   * @return bool return true if switched to sub
   */
  private function switchToSubIfSignsAreDifferent( BigInteger $other ): bool
  {
    if ($this->getSign () != $other->getSign ()) {
      if ($this->compareTo ($other) == self::THIS_IS_BIGGER) {
        $other->switchSign ();
        $this->sub ($other);
        $other->switchSign ();
        return true;
      } else {
        $this->switchSign ();
        $this->sub ($other);
        $this->switchSign ();
        return true;
      }
    }

    return false;
  }

  /**
   * This method return true if switched to add
   *
   * @param BigInteger $other
   * @return bool true if switched to add
   */
  private function switchToAddIfSignsAreDifferent( BigInteger $other ): bool
  {
    if ($this->getSign () != $other->getSign ()) {
      if ($this->getSign () == self::SIGN_NEGATIVE) {
        $this->switchSign ();
        $this->add ($other);
        $this->switchSign ();
        return true;
      } elseif ($other->getSign () == self::SIGN_NEGATIVE) {
        $other->switchSign ();
        $this->add ($other);
        $other->switchSign ();
        return true;
      }
    }
    return false;
  }

  private function trimLeadingZeroes()
  {
    $this->digits = str_split (
      rtrim (
        implode ("", $this->digits), "0"
      )
    );
  }

  /**
   * Set to zero if both are the same
   *
   * @param BigInteger $other
   * @return bool true if set to zero
   */
  private function ifBothAreSameSetToZero( BigInteger $other )
  {
    if ($this->compareTo ($other) == self::BOTH_ARE_EQUAL) {
      $this->digits = ["0"];
      $this->sign = self::SIGN_ZERO;
      return true;
    }
    return false;
  }

  /**
   * @param BigInteger $other
   * @param bool $switchSign
   * @return bool true if other value was taken
   */
  private function ifThisIsZeroTakeOtherValue( BigInteger $other, bool $switchSign = false )
  {
    if ($this->getSign () == self::SIGN_ZERO) {
      $this->sign = $other->getSign ();
      if ($switchSign)
        $this->switchSign ();
      $this->digits = $other->getDigits ();
      return true;
    }
    return false;
  }

  /**
   * @param BigInteger $other
   * @return int
   */
  private function identifyBiggerBySign( BigInteger $other ): int
  {
    if ($this->getSign () > $other->getSign ())
      return self::THIS_IS_BIGGER;
    elseif ($this->getSign () < $other->getSign ())
      return self::THE_OTHER_ONE_IS_BIGGER;

    return self::BOTH_ARE_EQUAL;
  }

  /**
   * @param BigInteger $other
   * @return int
   */
  private function identifyBiggerByDigitCount( BigInteger $other )
  {
    $differenceDigitsCount = count ($this->getDigits ()) - count ($other->getDigits ());
    if ($differenceDigitsCount > 0)
      return self::THIS_IS_BIGGER;
    elseif ($differenceDigitsCount < 0)
      return self::THE_OTHER_ONE_IS_BIGGER;

    return self::BOTH_ARE_EQUAL;
  }

  /**
   * @param $thisDigits
   * @param $otherDigits
   * @return int
   */
  private function compareDigitByDigit( BigInteger $other ): int
  {
    $thisDigits = $this->getDigits ();
    $otherDigits = $other->getDigits ();

    for ($i = count ($thisDigits) - 1; $i >= 0; $i--) {
      if ($thisDigits[$i] > $otherDigits[$i]) {
        return self::THIS_IS_BIGGER;
      } elseif ($thisDigits[$i] < $otherDigits[$i]) {
        return self::THE_OTHER_ONE_IS_BIGGER;
      }
    }

    return self::BOTH_ARE_EQUAL;
  }

  /**
   * @param $thisDigits
   * @param $otherDigits
   */
  private function addDigitByDigit( BigInteger $other ): void
  {
    $thisDigits = $this->getDigits ();
    $otherDigits = $other->getDigits ();

    $this->makeDigitsSameLength ($thisDigits, $otherDigits);

    $result = [];
    $carry = 0;

    // add digit by digit
    for ($i = 0; $i < count ($thisDigits); $i++) {
      $result[] = ($thisDigits[$i] + $otherDigits[$i] + $carry) % 10;
      $carry = floor (($thisDigits[$i] + $otherDigits[$i] + $carry) / 10);
    }

    if ($carry)
      $result[] = $carry;

    $this->digits = $result;
  }

  /**
   * @param BigInteger $other
   */
  private function subDigitByDigit( BigInteger $other ): void
  {
    $thisDigits = $this->getDigits ();
    $otherDigits = $other->getDigits ();

    if ($this->compareTo ($other) == self::THE_OTHER_ONE_IS_BIGGER) {
      $this->switchSign ();
      $this->swapValues ($thisDigits, $otherDigits);
    }

    $this->makeDigitsSameLength ($thisDigits, $otherDigits);

    $borrow = 0;
    $result = [];

    // subtract digit by digit
    for ($i = 0; $i < count ($thisDigits); $i++) {
      $difference = $thisDigits[$i] - $otherDigits[$i] - $borrow;
      if ($difference < 0) {
        $borrow = 1;
        $difference += 10;
      } else {
        $borrow = 0;
      }
      $result[] = $difference;
    }

    $this->digits = $result;
  }

  /**
   * @param $value1
   * @param $value2
   */
  private function swapValues( &$value1, &$value2 ): void
  {
      $digitsTmp = $value1;
      $value1 = $value2;
      $value2 = $digitsTmp;
  }
}
