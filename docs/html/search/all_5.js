var searchData =
    [
        ['setnumber_9', ['setNumber', ['../class_big_integer.html#ab33707c7a24910331a5f099b7bd1e0c4', 1, 'BigInteger']]],
        ['sign_5fnegative_10', ['SIGN_NEGATIVE', ['../class_big_integer.html#a479a7149f764441a81c586a9936c98b8', 1, 'BigInteger']]],
        ['sign_5fpositive_11', ['SIGN_POSITIVE', ['../class_big_integer.html#ab171fc84e94ef79ef17fd8ca8b12a3fa', 1, 'BigInteger']]],
        ['sign_5fzero_12', ['SIGN_ZERO', ['../class_big_integer.html#a232ddf3248641e8751b14331f632f13a', 1, 'BigInteger']]],
        ['sub_13', ['sub', ['../class_big_integer.html#a2117102b6c58f529641597e1538e255c', 1, 'BigInteger']]],
        ['switchsign_14', ['switchSign', ['../class_big_integer.html#adb209f57992374ace420e4ae4d8e6542', 1, 'BigInteger']]]
    ];
