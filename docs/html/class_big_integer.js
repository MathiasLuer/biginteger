var class_big_integer =
    [
        ["__construct", "class_big_integer.html#ac96312c5b4579b9dd445b979eafdcbe1", null],
        ["add", "class_big_integer.html#a43bbd89e60dce9b9117e3686bec4def3", null],
        ["compareTo", "class_big_integer.html#a57d0b59c608c41bc0a5acb62011e924a", null],
        ["getDigits", "class_big_integer.html#a04d451cdd2c9fdace655ec14fa9bc7a8", null],
        ["getNumber", "class_big_integer.html#a489ab44b15f7f39df2904e0bcdfc8955", null],
        ["getSign", "class_big_integer.html#a282c839169dc16e5d8bc47e8ac0f202b", null],
        ["setNumber", "class_big_integer.html#ab33707c7a24910331a5f099b7bd1e0c4", null],
        ["sub", "class_big_integer.html#a2117102b6c58f529641597e1538e255c", null],
        ["switchSign", "class_big_integer.html#adb209f57992374ace420e4ae4d8e6542", null],
        ["BOTH_ARE_EQUAL", "class_big_integer.html#aebaae95a108c8a2c19bfb88f5dcd9411", null],
        ["SIGN_NEGATIVE", "class_big_integer.html#a479a7149f764441a81c586a9936c98b8", null],
        ["SIGN_POSITIVE", "class_big_integer.html#ab171fc84e94ef79ef17fd8ca8b12a3fa", null],
        ["SIGN_ZERO", "class_big_integer.html#a232ddf3248641e8751b14331f632f13a", null],
        ["THE_OTHER_ONE_IS_BIGGER", "class_big_integer.html#a46e50d3c8664d6d1bcc78a516a19a177", null],
        ["THIS_IS_BIGGER", "class_big_integer.html#af921ee58cb776d6608e834a13655e1f2", null]
    ];